# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 16:55:11 2019

@author: Benjamin
"""
import re
import operator
import collections
import sys

playerInfo = []
playerBattedStats = {}
playerhitsStats = {}
#battingAve = {}

if len(sys.argv) < 2 or sys.argv[1] not in {'cardinals-1930.txt','cardinals-1940.txt','cardinals-1941.txt','cardinals-1942.txt','cardinals-1943.txt','cardinals-1944.txt'}:
    print('usage: baseball.py <cardinals-19xx.txt>')
    exit();
filename = sys.argv[1]

#(?P<player>^[\w]+[\s][\w\']+)[\s]\bbatted\b[\s](?P<batted>[\d]{1,2})[\s\w]+\bwith\b[\s](?P<hits>[\d]{1,2})
def findPlayerInfo(aStr, aDict, bDict):
    stats_regex = re.compile(r"(?P<player>^[\w]+[\s][\w\']+)[\s]\bbatted\b[\s](?P<batted>[\d]{1,2})[\s\w]+\bwith\b[\s](?P<hits>[\d]{1,2})")
    match = stats_regex.search(aStr)
    if match is not None:
        if match.group('player') in aDict:
            aDict[match.group('player')] += int(match.group('batted'))
            bDict[match.group('player')] += int(match.group('hits'))
        else:
            aDict[match.group('player')] = int(match.group('batted'))   
            bDict[match.group('player')] = int(match.group('hits'))
      
with open(filename, encoding="utf8") as file:
    for row in file:
        findPlayerInfo(row, playerBattedStats, playerhitsStats)

battingAve = {k:round(v/playerBattedStats[k], 3) for k,v in playerhitsStats.items()}

sorted_x = sorted(battingAve.items(), key=operator.itemgetter(1), reverse=True)
sorted_dict = collections.OrderedDict(sorted_x)
for k,v in sorted_dict.items():
    print(k+': {}'.format(sorted_dict[k]))
# need to use %.3f to show 3 decimal places at all times.   
    
  
    

        
        
    
